# -*- coding: utf-8 -*-
"""
#! -*- coding: utf-8 -*-
# 用CRF做中文命名实体识别
# 数据集 http://s3.bmio.net/kashgari/china-people-daily-ner-corpus.tar.gz
# 实测验证集的F1可以到96.18%，测试集的F1可以到95.35%
"""
import os
import numpy as np
from bert4keras.backend import keras, K
from bert4keras.models import build_transformer_model
from bert4keras.tokenizers import Tokenizer
from bert4keras.optimizers import Adam
from bert4keras.snippets import sequence_padding, DataGenerator
from bert4keras.snippets import open
from bert4keras.layers import ConditionalRandomField
from keras.layers import Dense
from keras.models import Model
from tqdm import tqdm


maxlen = 128
epochs = 10
batch_size = 32
bert_layers = 6
learing_rate = 1e-5  # bert_layers越小，学习率应该要越大
crf_lr_multiplier = 1000  # 必要时扩大CRF层的学习率

# bert配置
project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../"
))
config_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_small/albert_config.json"))
checkpoint_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_small/model.ckpt-best"
    ))
dict_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_small/vocab_chinese.txt"
    ))
model_path = os.path.normpath(os.path.join(
    project_dir, "models/best_model.weights"
))


def load_data(filename):
    D = []
    with open(filename, encoding='utf-8') as f:
        f = f.read()
        for l in f.split('\n\n'):
            if not l:
                continue
            d, last_flag = [], ''
            for c in l.split('\n'):
                try:
                    char, this_flag = c.split(' ')
                except:
                    continue
                if this_flag == 'O' and last_flag == 'O':
                    d[-1][0] += char
                elif this_flag == 'O' and last_flag != 'O':
                    d.append([char, 'O'])
                elif this_flag[:1] == 'B':
                    d.append([char, this_flag[2:]])
                else:
                    d[-1][0] += char
                last_flag = this_flag
            D.append(d)
    return D


# 标注数据
train_data = load_data(os.path.normpath(os.path.join(
    project_dir, 'data/beauty_robot/train.txt')))
valid_data = load_data(os.path.normpath(os.path.join(
    project_dir, 'data/beauty_robot/dev.txt')))
test_data = load_data(os.path.normpath(os.path.join(
    project_dir, 'data/beauty_robot/test.txt')))

# 建立分词器
tokenizer = Tokenizer(dict_path, do_lower_case=True)

# 类别映射
# labels = ['PER', 'LOC', 'ORG']
labels = [
    '补水',
    '产品',
    '长痘痘',
    '成分',
    '防晒/隔离类',
    '肤质',
    '隔离',
    '黑头',
    '化妆水',
    '季节',
    '洁面类',
    '精华',
    '抗衰老',
    '抗氧化',
    '控油',
    '脸部部位',
    '毛孔',
    '美白亮肤',
    '面部部位',
    '面膜',
    '面霜',
    '年龄',
    '品牌',
    '其他问题',
    '祛斑',
    '祛痘',
    '去角质',
    '乳液',
    '色素斑',
    '深层清洁',
    '收敛',
    '舒缓抗敏',
    '细纹',
    '卸妆水',
    '修复',
    '眼部',
    '眼部护理',
    '原液',
    '孕期',
]
id2label = dict(enumerate(labels))
label2id = {j: i for i, j in id2label.items()}
num_labels = len(labels) * 2 + 1


class data_generator(DataGenerator):
    """数据生成器
    """
    def __iter__(self, random=False):
        batch_token_ids, batch_segment_ids, batch_labels = [], [], []
        for is_end, item in self.sample(random):
            token_ids, labels = [tokenizer._token_start_id], [0]
            for w, l in item:
                w_token_ids = tokenizer.encode(w)[0][1:-1]
                if len(token_ids) + len(w_token_ids) < maxlen:
                    token_ids += w_token_ids
                    if l == 'O':
                        labels += [0] * len(w_token_ids)
                    else:
                        B = label2id[l] * 2 + 1
                        I = label2id[l] * 2 + 2
                        labels += ([B] + [I] * (len(w_token_ids) - 1))
                else:
                    break
            token_ids += [tokenizer._token_end_id]
            labels += [0]
            segment_ids = [0] * len(token_ids)
            batch_token_ids.append(token_ids)
            batch_segment_ids.append(segment_ids)
            batch_labels.append(labels)
            if len(batch_token_ids) == self.batch_size or is_end:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)
                batch_labels = sequence_padding(batch_labels)
                yield [batch_token_ids, batch_segment_ids], batch_labels
                batch_token_ids, batch_segment_ids, batch_labels = [], [], []


model = build_transformer_model(
    config_path,
    checkpoint_path,
    model='albert',
)
output_layer = 'Transformer-FeedForward-Norm'
output = model.get_layer(output_layer).get_output_at(bert_layers - 1)

output = Dense(num_labels)(output)
CRF = ConditionalRandomField(lr_multiplier=crf_lr_multiplier)
output = CRF(output)

model = Model(model.input, output)
model.summary()

model.compile(
    loss=CRF.sparse_loss,
    optimizer=Adam(learing_rate),
    metrics=[CRF.sparse_accuracy]
)


def viterbi_decode(nodes, trans):
    """Viterbi算法求最优路径
    其中nodes.shape=[seq_len, num_labels],
        trans.shape=[num_labels, num_labels].
    """
    labels = np.arange(num_labels).reshape((1, -1))
    scores = nodes[0].reshape((-1, 1))
    scores[1:] -= np.inf  # 第一个标签必然是0
    paths = labels
    for l in range(1, len(nodes)):
        M = scores + trans + nodes[l].reshape((1, -1))
        idxs = M.argmax(0)
        scores = M.max(0).reshape((-1, 1))
        paths = np.concatenate([paths[:, idxs], labels], 0)
    return paths[:, scores[:, 0].argmax()]


def named_entity_recognize(text):
    """命名实体识别函数
    """
    tokens = tokenizer.tokenize(text)
    while len(tokens) > 512:
        tokens.pop(-2)
    mapping = tokenizer.rematch(text, tokens)
    token_ids = tokenizer.tokens_to_ids(tokens)
    segment_ids = [0] * len(token_ids)
    nodes = model.predict([[token_ids], [segment_ids]])[0]
    trans = K.eval(CRF.trans)
    labels = viterbi_decode(nodes, trans)
    entities, starting = [], False
    for i, label in enumerate(labels):
        if label > 0:
            if label % 2 == 1:
                starting = True
                entities.append([[i], id2label[(label - 1) // 2]])
            elif starting:
                entities[-1][0].append(i)
            else:
                starting = False
        else:
            starting = False

    return [
        (text[mapping[w[0]][0]:mapping[w[-1]][-1] + 1], l) for w, l in entities
    ]


def evaluate(data):
    """评测函数
    """
    X, Y, Z = 1e-10, 1e-10, 1e-10
    for d in tqdm(data):
        try:
            text = ''.join([i[0] for i in d])
            R = set(named_entity_recognize(text))
            T = set([tuple(i) for i in d if i[1] != 'O'])
            X += len(R & T)
            Y += len(R)
            Z += len(T)
        except Exception as e:
            print(e.__str__())
    f1, precision, recall = 2 * X / (Y + Z), X / Y, X / Z
    return f1, precision, recall


class Evaluate(keras.callbacks.Callback):
    def __init__(self):
        self.best_val_f1 = 0

    def on_epoch_end(self, epoch, logs=None):
        trans = K.eval(CRF.trans)
        print(trans)
        f1, precision, recall = evaluate(valid_data)
        # 保存最优
        if f1 >= self.best_val_f1:
            self.best_val_f1 = f1
            model.save_weights(model_path)
        print(
            'valid:  f1: %.5f, precision: %.5f, recall: %.5f, best f1: %.5f\n' %
            (f1, precision, recall, self.best_val_f1)
        )
        f1, precision, recall = evaluate(test_data)
        print(
            'test:  f1: %.5f, precision: %.5f, recall: %.5f\n' %
            (f1, precision, recall)
        )


def _load_model():
    """
    训练模型
    :return:
    """
    if not os.path.exists(model_path):
        evaluator = Evaluate()
        train_generator = data_generator(train_data, batch_size)

        model.fit_generator(
            train_generator.forfit(),
            steps_per_epoch=len(train_generator),
            epochs=epochs,
            callbacks=[evaluator]
        )
    else:
        model.load_weights(model_path)


def ner(text):
    """
    ner外部调用接口
    :return:
    """
    _load_model()
    return named_entity_recognize(text)


if __name__ == "__main__":
    import timeit
    y = "y"
    while "y" == y:
        t = input("请输入：")
        ss = timeit.default_timer()
        result = ner(t)
        ee = timeit.default_timer()
        print("结果: {}, 耗时: {:.5f}".format(result, ee - ss))
        y = input("继续/y, 退出/n ?")
